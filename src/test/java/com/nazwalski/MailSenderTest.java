package com.nazwalski;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.nazwalski.repository.EmailRepository;
import com.nazwalski.service.MailSender;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MailSenderTest {

	@MockBean
	EmailRepository emailRepository;
	
	@MockBean
	MailSender mailSender;
	
	@Test
	public void sendEmailsFromDatabaseTest() {

		when(mailSender.sendMessage(anyString(), anyString(), anyString())).thenReturn(true);

		// Email email = new Email();
		// email.setContent("test");
		// email.setEmail("test@test.com");
		// email.setStatus(EmailStatus.NEW);
		// email.setTitle("testTitle");
		// email.setTryCounter(0);
		//
		// List<Email> emails = new ArrayList<Email>();
		// emails.add(email);
		//
		// List<Email> empty = null;
		//
		// when(emailRepository.findByStatus(EmailStatus.NEW)).thenReturn(emails);
		// when(emailRepository.findByStatus(EmailStatus.RETRY)).thenReturn(empty);
		//
		// mailSender.sendEmailsFromDatabase();

		assertTrue(mailSender.sendMessage("testContent", "test@test.com", "testTitle"));
		
		// assertThat(email.getStatus().equals(EmailStatus.SUCCESS));

	}

}