package com.nazwalski.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import com.nazwalski.model.Email;
import com.nazwalski.model.EmailStatus;

@Transactional
public interface EmailRepository extends CrudRepository<Email, Long> {

	@Transactional
	List<Email> findByStatus(EmailStatus status);
}
