package com.nazwalski.service;

import java.util.List;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.nazwalski.model.Email;
import com.nazwalski.model.EmailStatus;
import com.nazwalski.repository.EmailRepository;

@Component
public class MailSender {
	
	@Autowired
	private EmailRepository emailRepository;

	@Value(value = "${mail.user}")
	private String user;
	
	@Value(value = "${mail.password}")
	private String password;
	
	@Value(value = "${mail.account}")
	private String account;
	
	@Value(value = "${mail.numberOfTries}")
	private Integer numberOfTries;
	
	@Value(value = "${mail.smtp.host}")
	private String mailSmtpHost;
	
	@Value(value = "${mail.smtp.socketFactory.port}")
	private String mailSmtpSocketFactoryPort;
	
	@Value(value = "${mail.smtp.port}")
	private String mailSmtpPort;
	
	@Value(value = "${mail.smtp.socketFactory.class}")
	private String mailSmtpSocketFactoryClass;
	
	@Value(value = "${mail.smtp.auth}")
	private String mailSmtpAuth;

	private static final Logger logger = LoggerFactory.getLogger(MailSender.class);

	@Scheduled(fixedDelayString = "${mail.triesFrequency}")
	@Transactional
	public void sendEmailsFromDatabase() {

		boolean isEmailSent = false;
		List<Email> emails = null;
		
		emails = emailRepository.findByStatus(EmailStatus.NEW);
		emails.addAll(emailRepository.findByStatus(EmailStatus.RETRY));

		for (Email email : emails) {
			
			isEmailSent = sendMessage(email.getContent(), email.getEmail(), email.getTitle());

			if (isEmailSent == true) {
				email.setStatus(EmailStatus.SUCCESS);
				email.setTryCounter(email.getTryCounter() + 1);	
			} 
			else if (email.getTryCounter() < numberOfTries - 1) {
				email.setStatus(EmailStatus.RETRY);
				email.setTryCounter(email.getTryCounter() + 1);
			} 
			else {
				email.setStatus(EmailStatus.FAILED);
				email.setTryCounter(email.getTryCounter() + 1);			
			}
			emailRepository.save(email);
		}
	}

	public boolean sendMessage(String content, String emailTo, String title) {

		Properties props = new Properties();
		props.put("mail.smtp.host", mailSmtpHost);
		props.put("mail.smtp.socketFactory.port", mailSmtpSocketFactoryPort);
		props.put("mail.smtp.socketFactory.class", mailSmtpSocketFactoryClass);
		props.put("mail.smtp.auth", mailSmtpAuth);
		props.put("mail.smtp.port", mailSmtpPort);

		Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(user, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(account));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(emailTo));
			message.setSubject(title);
			message.setText(content);

			Transport.send(message);

		} catch (MessagingException e) {
			logger.error("Unable to send email.\n" + e);
			return false;
		}
		
		logger.info("Email to: " + emailTo + " sent successfully");
		return true;
	}
}
