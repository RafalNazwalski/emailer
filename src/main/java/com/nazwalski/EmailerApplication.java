package com.nazwalski;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class EmailerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmailerApplication.class, args);
	}
}
