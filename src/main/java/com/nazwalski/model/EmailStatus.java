package com.nazwalski.model;

public enum EmailStatus {

	NEW("NEW"),
	RETRY("RETRY"),
	FAILED("FAILED"),
	SUCCESS("SUCCESS");
	
	private String label;
	
	private EmailStatus(String name){
		this.label = name;
	}
	
	public String toString(){
		return label;
	}
}
