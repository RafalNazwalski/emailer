package com.nazwalski.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Rafal Nazwalski on 2017-03-23.
 */
@Entity
@Table(name = "Email")
public class Email {

	@Id
	@GeneratedValue
    private Long Id;
	
    @Column(name = "email")
    private String email;

    @Column(name = "Title")
    private String title;

    @Column(name = "Content")
    private String content;

    @Column(name = "TryCounter")
    private int tryCounter;

    @Column(name = "Status")
    @Enumerated(EnumType.STRING)
    private EmailStatus status;
    
    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getTryCounter() {
        return tryCounter;
    }

    public void setTryCounter(int tryCounter) {
        this.tryCounter = tryCounter;
    }

    public EmailStatus getStatus() {
        return status;
    }

    public void setStatus(EmailStatus status) {
        this.status = status;
    }

}
