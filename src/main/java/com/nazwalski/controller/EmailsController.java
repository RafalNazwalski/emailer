package com.nazwalski.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.nazwalski.model.Email;
import com.nazwalski.model.EmailStatus;
import com.nazwalski.repository.EmailRepository;

/**
 * Created by Rafal Nazwalski on 2017-03-23.
 */
@org.springframework.web.bind.annotation.RestController
public class EmailsController {
	
	private static final Logger logger = LoggerFactory.getLogger(EmailsController.class);

	@Autowired
	private EmailRepository emailRepository;
	
	@Transactional
	@RequestMapping(value = "/get-emails", method = RequestMethod.POST, consumes = "application/json")
	public void addEmailsToDatabase(@RequestBody List<Email> emails) {

		ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
		ObjectMapper mapper = new ObjectMapper();

		for (Email oneEmail : emails) {

			try {
				logger.debug("Printing emails: \n" + ow.writeValueAsString(oneEmail));	
				
			} catch (JsonProcessingException e1) {
				logger.error("Unable to process json. \n" + e1);
			}
			try {
				Email email = mapper.readValue(ow.writeValueAsString(oneEmail), Email.class);
				email.setStatus(EmailStatus.NEW);
				email.setTryCounter(0);
				emailRepository.save(email);
				
			} catch (IOException e) {
				logger.error("Unable to read / save emailRepository.\n" + e);
			}

		}

	}
}
